package dofus.server.game.job.maging;

import dofus.server.game.game.world.World;

import java.util.ArrayList;

public class BreakingObject {

    private ArrayList<World.Couple<Integer, Integer>> objects = new ArrayList<>();
    private int count = 0;
    private boolean stop = false;

    public ArrayList<World.Couple<Integer, Integer>> getObjects() {
        return objects;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public synchronized int addObject(int id, int quantity) {
        World.Couple<Integer, Integer> couple = this.search(id);

        if (couple == null) {
            this.objects.add(new World.Couple<>(id, quantity));
            return quantity;
        } else {
            couple.second += quantity;
            return couple.second;
        }
    }

    public synchronized int removeObject(int id, int quantity) {
        World.Couple<Integer, Integer> couple = this.search(id);

        if (couple != null) {
            if (quantity > couple.second) {
                this.objects.remove(couple);
                return quantity;
            } else {
                couple.second -= quantity;
                if (couple.second <= 0) {
                    this.objects.remove(couple);
                    return 0;
                }
                return couple.second;
            }
        }
        return 0;
    }

    public World.Couple<Integer, Integer> search(int id) {
        for (World.Couple<Integer, Integer> couple : this.objects)
            if (couple.first == id)
                return couple;
        return null;
    }
}