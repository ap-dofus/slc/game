package dofus.server.game.fight.ia;

import dofus.server.game.fight.Fight;
import dofus.server.game.fight.Fighter;
//import org.starloco.locos.util.TimerWaiter;

/**
 * Created by Locos on 18/09/2015.
 */
public interface IA {

    Fight getFight();

    Fighter getFighter();

    boolean isStop();

    void setStop(boolean stop);

    void addNext(Runnable runnable, Integer time);

    void apply();

    void endTurn();
}
