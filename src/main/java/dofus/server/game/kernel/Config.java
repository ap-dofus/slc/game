package dofus.server.game.kernel;

import dofus.server.game.database.Database;
import dofus.server.game.util.Points;

import java.io.*;
import java.util.*;

public class Config {
    private static Config singleton = null;


    public class Connectable{
        public final String host;
        public final int port;

        public Connectable(String host, int port) {
            this.host = host;
            this.port = port;
        }
    }

    public class Authable extends Connectable{
        public final String username, password;

        public Authable(String host, int port, String username, String password) {
            super(host, port);
            this.username = username;
            this.password = password;
        }
    }
    public class DB extends Authable{
        public final String name;

        public DB(String host, int port, String username, String password, String name) {
            super(host, port, username, password);
            this.name = name;
        }
    }
    public class Identifier{
        public final int id;
        public final String name, key;

        public Identifier(int id, String name, String key) {
            this.id = id;
            this.name = name;
            this.key = key;
        }
    }
    public class Rates{
        public final int xp, job, drop, kamas, fm, honor, erosion;

        public Rates(int xp, int job, int drop, int kamas, int fm, int honor, int erosion) {
            this.xp = xp;
            this.job = job;
            this.drop = drop;
            this.kamas = kamas;
            this.fm = fm;
            this.honor = honor;
            this.erosion = erosion;
        }
    }
    public class Events{
        public final boolean halloween, noel;

        public Events(boolean halloween, boolean noel) {
            this.halloween = halloween;
            this.noel = noel;
        }
    }
    public class Delays{
        public final int ai, aiMvtCell, aiMvtFlat, craft, action;

        public Delays(int ai, int aiMvtCell, int aiMvtFlat, int craft, int action) {
            this.ai = ai;
            this.aiMvtCell = aiMvtCell;
            this.aiMvtFlat = aiMvtFlat;
            this.craft = craft;
            this.action = action;
        }
    }
    public class PlayerStart{
        public final Map.Entry<Short, Short> location;
        public final int level, kamas;
        public final boolean zaap, emotes;

        public PlayerStart(Map.Entry<Short, Short> location, int level, int kamas, boolean zaap, boolean emotes) {
            this.location = location;
            this.level = level;
            this.kamas = kamas;
            this.zaap = zaap;
            this.emotes = emotes;
        }
    }
    public class Modes{
        public final boolean heroic, subscription;

        public Modes(boolean heroic, boolean subscription) {
            this.heroic = heroic;
            this.subscription = subscription;
        }
    }

    private class ConfigParam {
        public Optional<String> value;

        public ConfigParam(String defaultValue) {
            this.value = Optional.of(defaultValue);
        }
        public ConfigParam() {
            this.value = Optional.empty();
        }
    }
    private class ParamParser extends HashMap<String, ConfigParam> {
        public ParamParser() {
            super();
            this.put("LISTENING_HOST", new ConfigParam("127.0.0.1"));
            this.put("LISTENING_PORT", new ConfigParam("5555"));
            this.put("LOBBY_HOST", new ConfigParam("127.0.0.1"));
            this.put("LOBBY_PORT", new ConfigParam("666"));
            this.put("DB_GAME_HOST", new ConfigParam("127.0.0.1"));
            this.put("DB_GAME_PORT", new ConfigParam("3306"));
            this.put("DB_GAME_USER", new ConfigParam("root"));
            this.put("DB_GAME_PASS", new ConfigParam());
            this.put("DB_GAME_NAME", new ConfigParam("game"));
            this.put("DB_CHARACTERS_HOST", new ConfigParam("127.0.0.1"));
            this.put("DB_CHARACTERS_PORT", new ConfigParam("3306"));
            this.put("DB_CHARACTERS_USER", new ConfigParam("root"));
            this.put("DB_CHARACTERS_PASS", new ConfigParam());
            this.put("DB_CHARACTERS_NAME", new ConfigParam("accounts"));
            this.put("SERVER_ID", new ConfigParam("601"));
            this.put("SERVER_KEY", new ConfigParam("eratz"));
            this.put("ONLY_LOCAL", new ConfigParam("false"));
            this.put("DEBUG", new ConfigParam("false"));
            this.put("DUMP_LOG", new ConfigParam("false"));
            this.put("AUTO_RESTART", new ConfigParam("false"));
            this.put("SUBSCRIBTION_ENABLED", new ConfigParam("false"));
            this.put("SPAWN_LOCATION", new ConfigParam("0, 0"));
            this.put("START_LEVEL", new ConfigParam("1"));
            this.put("START_KAMAS", new ConfigParam("0"));
            this.put("ALL_ZAAP", new ConfigParam("false"));
            this.put("ALL_EMOTE", new ConfigParam("false"));
            this.put("ALLOW_MULE_PVP", new ConfigParam("false"));
            this.put("HEROIC", new ConfigParam("false"));
            this.put("HALLOWEEN", new ConfigParam("false"));
            this.put("NOEL", new ConfigParam("false"));
            this.put("RATE_XP", new ConfigParam("1"));
            this.put("RATE_DROP", new ConfigParam("1"));
            this.put("RATE_KAMAS", new ConfigParam("1"));
            this.put("RATE_JOB", new ConfigParam("1"));
            this.put("RATE_FM", new ConfigParam("1"));
            this.put("RATE_HONOR", new ConfigParam("1"));
            this.put("RATE_EROSION", new ConfigParam("10"));
            this.put("DELAY_AI", new ConfigParam("100"));
            this.put("DELAY_AI_MVT_CELL", new ConfigParam("180"));
            this.put("DELAY_AI_MVT_FLAT", new ConfigParam("700"));
            this.put("DELAY_CRAFT", new ConfigParam("200"));
            this.put("DELAY_ACTION", new ConfigParam("5"));
            this.put("MOTD", new ConfigParam("Bienvenue sur <b>StarLoco</b> !"));
            this.put("MESSAGE_COLOR", new ConfigParam("B9121B"));
            this.put("VOTE_URL", new ConfigParam());
        }

        public void parse(BufferedReader reader) throws IOException {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.split("=").length != 2)
                    continue;

                String[] splitted = line.split("=");

                get(splitted[0].trim().replace(" ", "").toUpperCase()).value
                        = Optional.of(splitted[1].trim());
            }
        }

        public Identifier getInfo(){
            String key = get("SERVER_KEY").value.get();
            return new Identifier(Integer.parseInt(get("SERVER_ID").value.get()),
                                  key.substring(0, 1).toUpperCase() + key.substring(1),
                                  key);
        }

        public Connectable getConn(){
            return new Connectable(get("LISTENING_HOST").value.get(),
                    Integer.parseInt(get("LISTENING_PORT").value.get()));
        }

        public Connectable getLobby(){
            return new Connectable(get("LOBBY_HOST").value.get(),
                    Integer.parseInt(get("LOBBY_PORT").value.get()));
        }

        private DB getDb(String prefix){
            return new DB(get("DB_" + prefix + "_HOST").value.get(),
                    Integer.parseInt(get("DB_" + prefix + "_PORT").value.get()),
                    get("DB_" + prefix + "_USER").value.get(),
                    get("DB_" + prefix + "_PASS").value.orElse(""),
                    get("DB_" + prefix + "_NAME").value.orElse(""));
        }
        public DB getGameDB(){
            return getDb("GAME");
        }
        public DB getCharactersDB(){
            return getDb("CHARACTERS");
        }

        private Map.Entry<Short, Short> getSpawnLocation(){
            String[] values = get("SPAWN_LOCATION").value.get().split("\\,");
            return new AbstractMap.SimpleEntry<>(Short.parseShort(values[0].trim()), Short.parseShort(values[1].trim()));
        }
        public PlayerStart getPlayerStart(){
            return new PlayerStart(getSpawnLocation(),
                    Integer.parseInt(get("START_LEVEL").value.get()),
                    Integer.parseInt(get("START_KAMAS").value.get()),
                    get("ALL_ZAAP").value.get().equals("true"),
                    get("ALL_EMOTE").value.get().equals("true"));
        }

        public Rates getRates(){
            return new Rates(Integer.parseInt(get("RATE_XP").value.get()),
                    Integer.parseInt(get("RATE_JOB").value.get()),
                    Integer.parseInt(get("RATE_DROP").value.get()),
                    Integer.parseInt(get("RATE_KAMAS").value.get()),
                    Integer.parseInt(get("RATE_FM").value.get()),
                    Integer.parseInt(get("RATE_HONOR").value.get()),
                    Integer.parseInt(get("RATE_EROSION").value.get()));
        }

        public Modes getModes(){
            return new Modes(get("HEROIC").value.get().equals("true"),
                    get("SUBSCRIBTION_ENABLED").value.get().equals("true"));
        }
        public Events getEvents(){
            return new Events(get("HALLOWEEN").value.get().equals("true"),
                    get("NOEL").value.get().equals("true"));
        }
        public Delays getDelays(){
            return new Delays(Integer.parseInt(get("DELAY_AI").value.get()),
                    Integer.parseInt(get("DELAY_AI_MVT_CELL").value.get()),
                    Integer.parseInt(get("DELAY_AI_MVT_FLAT").value.get()),
                    Integer.parseInt(get("DELAY_CRAFT").value.get()),
                    Integer.parseInt(get("DELAY_ACTION").value.get()));
        }

        public boolean getOnlyLocal(){
            return get("ONLY_LOCAL").value.get().equals("true");
        }
        public boolean getDebug(){
            return get("DEBUG").value.get().equals("true");
        }
        public boolean getDumpLog(){
            return get("DUMP_LOG").value.get().equals("true");
        }
        public boolean getAutoRestart(){
            return get("AUTO_RESTART").value.get().equals("true");
        }
        public boolean getAllowMulePVP(){
            return get("ALLOW_MULE_PVP").value.get().equals("true");
        }
        public String getMOTD(){
            return get("MOTD").value.get();
        }
        public String getMessageColor(){
            return get("MESSAGE_COLOR").value.get();
        }
        public Optional<String> getVoteUrl(){
            return get("VOTE_URL").value;
        }
    }


    public final long startTime = System.currentTimeMillis();
    public final Connectable lobby, conn;
    public final DB gameDB, charactersDB;
    public final Identifier info;
    public final Rates rates;
    public final Delays delays;
    public final Events events;
    public final PlayerStart playerStart;
    public final Modes modes;

    public final boolean mulePVP, onlyLocal, dumpLogs, autoRestart;
    public boolean debug;

    public final Optional<String> voteUrl;
    public final String motd, messageColor;


    public Config() {
        ParamParser params = new ParamParser();
        try{
            FileReader file = new FileReader("config.txt");
            try {
                params.parse(new BufferedReader(new FileReader("config.txt")));
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {}

        info = params.getInfo();
        conn = params.getConn();
        lobby = params.getLobby();
        gameDB = params.getGameDB();
        charactersDB = params.getCharactersDB();
        rates = params.getRates();
        delays = params.getDelays();
        events = params.getEvents();
        playerStart = params.getPlayerStart();
        modes = params.getModes();
        mulePVP = params.getAllowMulePVP();
        onlyLocal = params.getOnlyLocal();
        debug = params.getDebug();
        dumpLogs = params.getDumpLog();
        autoRestart = params.getAutoRestart();
        voteUrl = params.getVoteUrl();
        motd = params.getMOTD();
        messageColor = params.getMessageColor();
    }

    public Points points = new Points() {
        @Override
        public int load(String user) {
            return Database.getStatics().getAccountData().loadPointsWithoutUsersDb(user);
        }

        @Override
        public void update(int id, int points) {
            Database.getStatics().getAccountData().updatePointsWithoutUsersDb(id, points);
        }
    };

    public static Config getInstance() {
        if (singleton == null)
            singleton = new Config();
        return singleton;
    }
}
