package dofus.server.game.database.dynamics.data;

import com.zaxxer.hikari.HikariDataSource;
import dofus.server.game.database.dynamics.AbstractDAO;
import dofus.server.game.quest.Quest;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestObjectiveData extends AbstractDAO<Quest.Quest_Objectif> {
    public QuestObjectiveData(HikariDataSource dataSource) {
        super(dataSource);
    }

    @Override
    public void load(Object obj) {
    }

    @Override
    public boolean update(Quest.Quest_Objectif obj) {
        return false;
    }

    public void load() {
        Result result = null;
        try {
            result = getData("SELECT * FROM quest_objectifs");
            ResultSet loc1 = result.resultSet;
            Quest.Quest_Objectif.questObjectifList.clear();
            while (loc1.next()) {
                Quest.Quest_Objectif qObjectif = new Quest.Quest_Objectif(loc1.getInt("id"), loc1.getInt("xp"), loc1.getInt("kamas"), loc1.getString("item"), loc1.getString("action"));
                Quest.Quest_Objectif.setQuest_Objectif(qObjectif);
            }
            close(result);
        } catch (SQLException e) {
            super.sendError("Quest_objectifData load", e);
        } finally {
            close(result);
        }
    }
}
