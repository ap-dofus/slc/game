package dofus.server.game.database.dynamics.data;

import com.zaxxer.hikari.HikariDataSource;
import dofus.server.game.area.map.entity.InteractiveObject;
import dofus.server.game.database.dynamics.AbstractDAO;
import dofus.server.game.game.world.World;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InteractiveObjectData extends
        AbstractDAO<InteractiveObject.InteractiveObjectTemplate> {
    public InteractiveObjectData(HikariDataSource dataSource) {
        super(dataSource);
    }

    @Override
    public void load(Object obj) {
    }

    @Override
    public boolean update(InteractiveObject.InteractiveObjectTemplate obj) {
        return false;
    }

    public void load() {
        Result result = null;
        try {
            result = getData("SELECT * from interactive_objects_data");
            ResultSet RS = result.resultSet;
            while (RS.next()) {
                World.world.addIOTemplate(new InteractiveObject.InteractiveObjectTemplate(RS.getInt("id"), RS.getInt("respawn"), RS.getInt("duration"), RS.getInt("unknow"), RS.getInt("walkable") == 1));
            }
        } catch (SQLException e) {
            super.sendError("Interactive_objects_dataData load", e);
        } finally {
            close(result);
        }
    }
}
