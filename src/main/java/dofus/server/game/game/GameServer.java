package dofus.server.game.game;

import dofus.server.game.client.Account;
import dofus.server.game.client.Player;
import dofus.server.game.game.world.World;
import dofus.server.game.kernel.Config;
import dofus.server.game.kernel.Main;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public final class GameServer {

    public static short MAX_PLAYERS = 700;
    private final ArrayList<Account> waitingClients = new ArrayList<>();

    private ServerSocket serverSocket;
    private final ExecutorService threadPool = Executors.newFixedThreadPool(MAX_PLAYERS);
    private boolean alive;
    private final ArrayList<GameHandler> clients = new ArrayList<>();


    public GameServer() {
        Main.gameServer = this;
    }

    public static String getServerTime() {
        return "BT" + (new Date().getTime() + 3600000 * 2);
    }

    public static void setState(int state) {
        if (Main.exchangeClient != null && Main.exchangeClient.getConnectFuture() != null && !Main.exchangeClient.getConnectFuture().isCanceled() && Main.exchangeClient.getConnectFuture().isConnected())
            Main.exchangeClient.send("SS" + state);
    }

    public static void a() {
    }


    public void initialize() {
        if (serverSocket != null)
            return;

        try {
            serverSocket = new ServerSocket(Config.getInstance().conn.port);
            alive = true;
        } catch (IOException e) {
            Main.logger.error("The address '" + Config.getInstance().conn.port + "' is already in use..");
            this.close();
            try {
                Thread.sleep(3000);
            } catch (Exception ignored) {
            }
            this.initialize();
        } finally {
            Main.logger.info("The game server started on address : " + Config.getInstance().conn.host + ":" + Config.getInstance().conn.port);
        }

        new Thread(this::listenToClients).start();

    }

    public void listenToClients() {
        try {
            while (alive) {
                Socket clientSocket = serverSocket.accept(); // new player just connected to the server
                GameHandler gameHandler = new GameHandler(clientSocket, this);
                clients.add(gameHandler);
                threadPool.submit(gameHandler::listenToClient);
            }
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
        }

    }

    public void close() {
        if (this.serverSocket == null) return;
        if (this.serverSocket.isClosed()) return;

        try {
            this.serverSocket.close();
        } catch (IOException e) {
            // ignore the error
        } finally {
            this.clients.stream().filter(GameHandler::isAlive).forEach(GameHandler::closeConnection);
            this.clients.clear();
            this.serverSocket = null;
            Main.logger.error("The game server was stopped.");
        }
    }

    public synchronized void removeHandler(GameHandler gameHandler) {
        this.clients.remove(gameHandler);
    }

    public ArrayList<GameClient> getClients() {
        return clients.stream().map(GameHandler::getGameClient).collect(Collectors.toCollection(ArrayList::new));
    }

    public int getPlayersNumberByIp() {
        ArrayList<String> IPS = new ArrayList<>();
        this.getClients().stream().filter(client -> client != null && client.getAccount() != null).forEach(client -> {
            String IP = client.getAccount().getCurrentIp();
            if (!IP.equalsIgnoreCase("") && !IPS.contains(IP)) IPS.add(IP);
        });
        return IPS.size();
    }

    public Account getWaitingAccount(int id) {
        for (Account account : this.waitingClients)
            if (account.getId() == id)
                return account;
        return null;
    }

    public void deleteWaitingAccount(Account account) {
        this.waitingClients.remove(account);
    }

    public void addWaitingAccount(Account account) {
        if (!this.waitingClients.contains(account)) this.waitingClients.add(account);
    }

    public void kickAll(boolean kickGm) {
        for (Player player : World.world.getOnlinePlayers()) {
            if (player != null && player.getGameClient() != null) {
                if (player.getGroupe() != null && !player.getGroupe().isPlayer() && kickGm)
                    continue;
                player.send("M04");
                player.getGameClient().kick();
            }
        }
    }
}
