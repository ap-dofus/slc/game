package dofus.server.game.game;

import dofus.server.game.game.world.World;
import dofus.server.game.kernel.Config;
import dofus.server.game.kernel.Main;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public final class GameHandler {

    private final GameClient gameClient;
    private final BufferedInputStream inputStream;
    private final PrintWriter outputStream;
    private boolean alive = true;
    private final Socket clientSocket;
    private final GameServer gameServer;

    public GameHandler(Socket clientSocket, GameServer gameServer) throws IOException {
        this.gameServer = gameServer;
        this.clientSocket = clientSocket;
        this.outputStream = new PrintWriter(clientSocket.getOutputStream());;
        this.inputStream = new BufferedInputStream(clientSocket.getInputStream());
        this.gameClient = new GameClient(this);
        Main.refreshTitle();
    }

    public String getClientIp() {
        return this.clientSocket.getRemoteSocketAddress().toString();
    }

    public GameClient getGameClient() {
        return gameClient;
    }

    public boolean isAlive() {
        return alive;
    }

    public void write(String packet){
        String playerName = gameClient!= null && gameClient.getPlayer() != null ? gameClient.getPlayer().getName() : "";
        if (Config.getInstance().debug)
            World.world.logger.trace("Server --> " + playerName + " --> " + packet);
        this.outputStream.print(packet.concat("\0"));
        this.outputStream.flush();
    }

    public void listenToClient() {
        try {
            int lenght = -1;
            byte[] bytes = new byte[1024];
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            while (((lenght = inputStream.read(bytes)) != -1) && alive) {
                byteArray.write(bytes, 0, lenght);
                if (inputStream.available() == 0) {
                    String tempPacket = byteArray.toString("UTF-8");
                    for (String packet : tempPacket.split("[\u0000\n\r]")) {
                        if (packet.isEmpty()) {
                            continue;
                        }
                        if (Config.getInstance().debug)
                            World.world.logger.trace(("Client --> " + (gameClient.getPlayer() == null ? "" : gameClient.getPlayer().getName()) + " --> " + packet));
                        gameClient.parsePacket(packet);
                    }
                    byteArray.reset();
                }
            }
        } catch (Exception ex) {
            World.world.logger.error("Error while processing received packets from clients: " + ex.getMessage());
        } finally {
            closeConnection();
        }

    }


    public void closeConnection() {
        try {
            if (inputStream != null) inputStream.close();
            if (outputStream != null) outputStream.close();
            if (clientSocket != null) clientSocket.close();
        } catch (Exception e) {
            // ignore error
        } finally {
            alive = false;
            if (gameClient != null) gameClient.disconnect(); // update database when player logs off
            this.gameServer.removeHandler(this);
        }
    }


}
