package dofus.server.game.game.scheduler.entity;

import dofus.server.game.client.Player;
import dofus.server.game.database.Database;
import dofus.server.game.game.scheduler.Updatable;
import dofus.server.game.game.world.World;

public class WorldPlayerOption extends Updatable {

    public final static Updatable updatable = new WorldPub(300000);

    public WorldPlayerOption(int wait) {
        super(wait);
    }

    @Override
    public void update() {
        if (this.verify()) {
            Database.getStatics().getAccountData().updateVoteAll();
            World.world.getOnlinePlayers().stream().filter(player -> player != null && player.isOnline()).forEach(Player::checkVote);
        }
    }

    @Override
    public Object get() {
        return null;
    }
}