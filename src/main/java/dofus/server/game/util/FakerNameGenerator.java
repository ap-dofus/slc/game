package dofus.server.game.util;

import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class FakerNameGenerator {

    private static final Faker FAKER_GENERATOR = new Faker(Locale.getDefault());
    private static final Random JAVA_RANDOM = new Random();

    public static String generateRandomName() {
        boolean useUsername = JAVA_RANDOM.nextBoolean();
        return useUsername ? generateUsername() : generateName();
    }


    private static String generateUsername() {
        return FAKER_GENERATOR.name().username().replace(".", "-");
    }

    private static String generateName() {
        boolean useHyphen = JAVA_RANDOM.nextBoolean();
        String firstName = FAKER_GENERATOR.name().firstName();
        return useHyphen ? firstName.concat("-").concat(FAKER_GENERATOR.name().lastName().toLowerCase(Locale.ROOT))
                : firstName;
    }

}
