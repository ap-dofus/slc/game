package dofus.server.game.util.lang.type;

import dofus.server.game.client.Player;
import dofus.server.game.kernel.Config;
import dofus.server.game.util.lang.AbstractLang;

/**
 * Created by Locos on 09/12/2015.
 */
public class French extends AbstractLang {

    public final static French singleton = new French();

    public static French getInstance() {
        return singleton;
    }

    private void initSentence(String sentence) {
        this.sentences.add(this.sentences.size(), sentence);
    }

    public void initialize() {
        initSentence("Votre canal général est désactivé.");
        initSentence("Les caractères point virgule, chevrons et tildé sont désactivés.");
        initSentence("Tu dois attendre encore #1 seconde(s).");
        initSentence("Vous avez activé le canal général.");
        initSentence("Vous avez désactivé le canal général.");
        initSentence("Liste des membres du staff connectés :");
        initSentence("Il n'y a aucun membre du staff connecté ou peut-être y a t'il la présence de Locos ?");
        initSentence("Vous n'êtes pas bloqué..");
        initSentence("<b>StarLoco - " + Config.getInstance().info.name + "</b>\nEn ligne depuis : #1j #2h #3m #4s.");
        initSentence("\nJoueurs en ligne : #1");
        initSentence("\nJoueurs uniques en ligne : #1");
        initSentence("\nRecord de connexion : #1");
        Player player = null;
        //            + "<b>.infos</b> - Permet d'obtenir des informations sur le serveur.\n"
        //+ "<b>.noall</b> - Permet de ne plus recevoir les messages du canal général."); index++;
        //+ "<b>.staff</b> - Permet de voir les membres du staff connectés.\n"
        initSentence("Les commandes disponnibles sont :\n"
                + "<b>.all</b> - <b>.noall</b> - Permet d'envoyer un message à tous les joueurs. (ex : .all Hey!)\n"
                + "<b>.deblo</b> - Permet de vous débloquer en vous téléportant à une cellule libre.\n"
                + "<b>.onboard</b> - Vous donne tout ce dont vous avez besoin pour effectuer des tests.\n"
                + "<b>.level</b> - Permet de fixer son level à une valeur. (Ex : .level 100)\n"
                + "<b>.restat</b> - Permet de remettre ses caractéristiques à 0.\n"
                + "<b>.parcho</b> - Permet de se parcho 101 dans tous les éléments.\n"
                + "<b>.boost</b> - Permet de booster ses caractéristiques plus vite. "
                + "(ex : .boost [vita/sagesse/force/intel/chance/agi] [x] pour booster x dans l'élément souhaité.) \n"
                + "<b>.spellmax</b> - Permet de monter ses sorts au niveau max.\n"
                + "<b>.jetmax</b> - Permet d'avoir le jet max sur un item. (ex : .jetmax all)\n"
                + "<b>.exo</b> - Permet d'exo un item PA ou PM. (ex : .exo cape pa)\n"
                + "<b>.fmcac</b> - Permet d'FM son cac dans un élément d'attaque. (ex : .fmcac feu)\n"
                + "<b>.start</b> - Permet de se téléporter à la map de départ.\n"
                + "<b>.poutch</b> - Permet de se téléporter au poutch.\n"
                + "<b>.maitre</b> - Permet de déplacer sa team à l'aide d'un seul terminal.\n"
                + "<b>.ipdrop</b> - Redirige tous les drops de votre team vers le maitre.\n"
                + "<b>.pass</b> - Permet de passer vos tours automatiquement en combat. \n"
                + "<b>.pvm</b> - Permet de se téléporter à la map pvm.\n"
                + "<b>.vie</b> - Permet de restaurer 100% de sa vie.\n"
                + "<b>.banque</b> - Permet d'ouvrir l'interface de sa banque.\n"
                + "<b>.transfert</b> - Permet de transférer toutes ses ressources en banque. (Lag après 30 items.)\n"
                + "<b>.enclos</b> - Permet de se téléporter à un enclos.\n"
                + "<b>.phoenix</b> - Permet de se téléporter à la statue du Phoenix.\n"
                + "<b>.ange</b> - Permet de passer en Alignement Bontarien.\n"
                + "<b>.demon</b> - Permet de passer en Alignement Brakmarien.\n"
                + "<b>.neutre</b> - Permet de passer en Alignement Neutre.\n");
        initSentence("Retrouvez les commandes en tapant .commandes ou .x dans le chat.");
        Config.getInstance().voteUrl.ifPresent(s -> this.sentences.add("Vous pouvez dès à présent voter, <b><a href='" + s + "'>clique ici</a></b> !"));
    }
}
