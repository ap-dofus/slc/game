# StarLoco Continued: Game-Server : Configuration

### File & Location
The configuration file should be called `config.txt`.

If you are using the .jar executable, this config file is to be found under the working directory.

If you are using the docker service, the file is to be mounted under: `/var/lib/dofus/config.txt` (ro)

Here is the example of an exhaustive useless config file : [examples/config.txt](../examples/config.txt).
It is exhaustive because every option is present, and useless because each (non-mandatory) option is affected the default associated value.

### Content (`VAR_NAME` [= <default_value>] : <info / comment>)
 * `SERVER_ID` = 601 : The id of the server (must match both an existing DB entry and an existing lang-files entry)
 * `SERVER_KEY` = eratz : The litteral key associated to the server id (must match both an existing DB and lang-files entries, correlated to the aformentionned id)


 * `LISTENING_HOST` = 127.0.0.1 : The IP / hostname clients will use to join the server. If you want your server to be accessible locally, use the local network (or Hamachi) address. If you want it to be accessible remotely, use a domain or a static IP. The default value (127.0.0.1) will only work for clients running on the same host as the server.
 * `LISTENING_PORT` = 5555 : The port clients will use to join the server. If you want your server to be available locally, a firewall rule must be added on the host. And if you want your server to be available remotely, the router must redirect traffic to the host on this port (port forwarding on different ports is not supported yet).


 * `LOBBY_HOST` = 127.0.0.1 : The IP / hostname on which to reach the lobby-server using the exchange protocol. You don't need to specify it (=default) if the game-server is running on the same host as the lobby-server.
 * `LOBBY_PORT` = 666 : The port on which to reach the lobby-server using the exchange protocol. You don't need to specify it if you used the default listening port (= didn't specify) for the exchange protocol on the lobby-server.


 * `DB_GAME_HOST` = 127.0.0.1 : The IP / hostname on which to reach the game database. You don't need to specify it (=default) if the game-server is running on the same host as the aforementioned database.
 * `DB_GAME_PORT` = 3306 : The port on which to reach the game database. You don't need to specify it if you used the default listening port for your Maria/MySQL database.
 * `DB_GAME_USER` = root : The user with which to reach the game database. You don't need to specify it if you want to use the root user (not recommended).
 * `DB_GAME_PASS`: The password for the aforementioned user (mandatory / no default).
 * `DB_GAME_NAME` = game : The name of the schema containing game data on the game database. You don't need to specify it if you called the schema "game".


 * `DB_CHARACTERS_HOST` = 127.0.0.1 : The IP / hostname on which to reach the characters database. You don't need to specify it (!=default) if the game-server is running on the same host as the aforementioned database.
 * `DB_CHARACTERS_HOST` = 3306 : The port on which to reach the characters database. You don't need to specify it if you used the default listening port for your Maria/MySQL database.
 * `DB_CHARACTERS_HOST` = root : The user with which to reach the characters database. You don't need to specify it if you want to use the root user (not recommended).
 * `DB_CHARACTERS_HOST`: The password for the aforementioned user (mandatory / no default).
 * `DB_CHARACTERS_HOST` = accounts : The name of the schema containing characters data on the characters database. You don't need to specify it if you called the schema "accounts".


 * `SPAWN_LOCATION` = 0, 0 : The <map_id, cell_id> location of spawn on the server. You don't need to specify it if you want to use the Anka-like class attributed Incarnam start.
 * `START_LEVEL` = 1 : The level of starting characters on the server. You don't need to specify it if you want to use the Anka-like level 1 start.
 * `START_KAMAS` = 0 : The inventory money of starting characters on the server. You don't need to specify it if you want to use the Anka-like level 0 kamas start.
 * `ALL_ZAAP` = false : Whether starting characters should know all ZAAPs or not. You don't need to specify it if you want to use the Anka-like start with no zaap known.
 * `ALL_EMOTE` = false : Whether starting characters should know all emotes or not. You don't need to specify it if you want to use the Anka-like start with no emotes known.


 * `ALLOW_MULE_PVP` = false : Whether players should be allowed to use their mules (= same IP) during PVP fights. You don't need to specify it if you want to keep the option disabled.
 * `HEROIC` = false : Whether characters should perma-die or not. You don't need to specify it if you want to keep the option disabled (classic non-heroic server).


 * `HALLOWEEN` = false : Whether the halloween event is active on the server or not. You don't need to specify it if you want to keep the option disabled.
 * `NOEL` = false : Whether the noel event is active on the server ot not. You don't need to specify it if you want to keep the option disabled.


 * `RATE_XP` = 1 : Linear experience-rate modifier. You don't need to specify it if you want to keep the Anka-like experience rate.
 * `RATE_DROP` = 1 : Linear drop-rate modifier. You don't need to specify it if you want to keep the Anka-like drop rate.
 * `RATE_JOB` = 1 : Linear job-experience-rate modifier. You don't need to specify it if you want to keep the Anka-like job-experience rate.
 * `RATE_FM` = 1 : Linear fm-success-rate modifier. You don't need to specify it if you want to keep the Anka-like fm-success rate.
 * `RATE_HONOR` = 1 : Linear honor-change-rate modifier. You don't need to specify it if you want to keep the Anka-like honor-change rate.
 * `RATE_EROSION` = 10 : TODO Document.


 * `DELAY_AI` = 100 : TODO Document.
 * `DELAY_AI_MVT_CELL` = 180 : TODO Document.
 * `DELAY_AI_MVT_FLAT` = 700 : TODO Document.
 * `DELAY_CRAFT` = 200 : TODO Document.
 * `DELAY_ACTION` = 5 : TODO Document.


 * `MOTD` = Bienvenue sur <b>StarLoco</b> ! : Message of the day, shown at login.


 * `ONLY_LOCAL` = false : Whether to only listen on local network or not. You don't need to specify it if you want to have your server remotely accessible.
 * `DEBUG` = false : Whether to dump trace level logging in the execution TTY or not. You don't need to specify it if you want to keep the option disabled.
 * `DUMP_LOG` = false : Whether to dump logging in files or not. You don't need to specify it if you want to keep the option disabled.
 * `AUTO_RESTART` = false : Whether to restart the server when the service failed or not. You don't need to specify it if you want to keep the option disabled.
 * `SUBSCRIBTION_ENABLED` = false : Whether the server is subscription-based or not. You don't need to specify it if you want to keep the option disabled.
 